<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//pre admina
Route::get('/admin/categories/create', [App\Http\Controllers\AdminController::class, 'createCategory'])->middleware(["adminRole"]);
Route::post('/admin/categories', [App\Http\Controllers\AdminController::class, 'storeCategory'])->middleware(["adminRole"]);

Route::get('/admin/categories', [App\Http\Controllers\AdminController::class, 'indexCategory'])->middleware(["adminRole"]); //zobrazenie kategórií
Route::get('/admin/categories/{category}/edit', [App\Http\Controllers\AdminController::class, 'editCategory'])->middleware(["adminRole"]); //zobrazenie upravy kategórie
Route::patch('/admin/categories/{category}', [App\Http\Controllers\AdminController::class, 'updateCategory'])->middleware(["adminRole"]); //uprava kategorie
Route::delete('/admin/categories/delete', [App\Http\Controllers\AdminController::class, 'destroyCategory'])->middleware(["adminRole"]); //softvymazanie kategŕie
Route::delete('/admin/categories/restore', [App\Http\Controllers\AdminController::class, 'restoreCategory'])->middleware(["adminRole"]); //obnovenie produktu

Route::get('/admin/orders/', [App\Http\Controllers\AdminController::class, 'indexOrder'])->middleware(["adminRole"]); //zobrazenie vsetkych objednavok
Route::get('/admin/orders/{order}', [App\Http\Controllers\AdminController::class, 'showOrder'])->middleware(["adminRole"]); //zobrazenie objednavky
Route::post('/admin/orders/', [App\Http\Controllers\AdminController::class, 'updateOrder'])->middleware(["adminRole"]); //uprava objednavky

Route::get('/admin/products', [App\Http\Controllers\AdminController::class, 'index'])->middleware(["adminRole"]); //zobrazenie produktov
Route::get('/admin/products/{product}/edit', [App\Http\Controllers\AdminController::class, 'edit'])->middleware(["adminRole"]); //zobrazenie upravy produktu
Route::patch('/admin/products/{product}', [App\Http\Controllers\AdminController::class, 'update'])->middleware(["adminRole"]); //uprava produktu
Route::delete('/admin/products/delete', [App\Http\Controllers\AdminController::class, 'destroy'])->middleware(["adminRole"]); //softvymazanie produktu
Route::delete('/admin/products/restore', [App\Http\Controllers\AdminController::class, 'restore'])->middleware(["adminRole"]); //obnovenie produktu

Route::get('/admin/products/create', [App\Http\Controllers\AdminController::class, 'create'])->middleware(["adminRole"]);
Route::post('/admin/products', [App\Http\Controllers\AdminController::class, 'store'])->middleware(["adminRole"]);

//produkty
Route::get('/', [App\Http\Controllers\ProductController::class, 'index']); //index stranky
Route::get('product/{product}', [App\Http\Controllers\ProductController::class, 'show']); //zobrazenie produktov
Route::get('product/category/{category}', [App\Http\Controllers\ProductController::class, 'showCategory']); //zobrazenie produktov v kategorii

//kosik
Route::get('/cart', [App\Http\Controllers\CartController::class, 'index']);//zobraz kosik
Route::post('/cart/{product}', [App\Http\Controllers\CartController::class, 'store']);//pridaj do kosika
Route::patch('/cart/{product}', [App\Http\Controllers\CartController::class, 'update']); //uprava kosika
Route::delete('/cart/{product}', [App\Http\Controllers\CartController::class, 'destroy']); //vymazanie produktu z kosika

//objednavky
Route::get('/orders', [App\Http\Controllers\OrderController::class, 'index']);
Route::post('/orders', [App\Http\Controllers\OrderController::class, 'store']);

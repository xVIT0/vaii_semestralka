<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $cart = [];
        $products_id = [];
        if(request()->session()->has("cart")) {
            $cart = session("cart");
            foreach ($cart as $key => $value) {
                if($key != "total" && !Product::find($key)->deleted) {
                    array_push($products_id, $key);
                }
            }

        }
        $products = Product::find($products_id);
        //dd($products);
        return view("cart.index", compact("products", "cart"));
    }

    public function store(Product $product) {
        if($product) {
            $cart = [];
            $cart['total'] = 0;
            $found = false;
            if(request()->session()->has("cart")) {
                $cartOld = request()->session()->get("cart");
                $totalPrice = 0;
                foreach($cartOld as $key => $value) {
                    if($key != "total") {
                        if($key == $product->id) {
                            $value['amount']++;
                            $found = true;
                        }
                        $cart[$key] = $value;
                        $totalPrice += $value['amount'] * $value['price'];
                    }
                }
                $cart["total"] = $totalPrice;

            }
            if(!$found) {
                $cart[$product->id] = [
                    "amount" => 1,
                    "price" => $product->price
                ];
                $cart["total"] += $product->price;
            }
            request()->session()->forget("cart");
            request()->session()->put("cart",$cart);

            /*$kosik = request()->session()->get("cart");
            foreach ($kosik as $i => $value) {
                if($i == "total") {
                    echo $i." ".$value . "<br>";
                }
                else {
                    echo $i." ".$value["amount"]." ".$value["price"]."<br>";
                }
            }*/

        }

        //return redirect('/cart');

        return "Success";

    }
    public function destroy(Product $product) {
        $cart = [];
        $cart["total"] = 0;
        if(request()->session()->has("cart")) {
            $cart_old = session("cart");
            foreach ($cart_old as $key => $value) {
                if($key != "total" && !Product::find($key)->deleted && $key != $product->id) {
                    $cart[$key] = $value;
                    $cart["total"] += $value["amount"] * $value["price"];
                }
            }
            request()->session()->forget("cart");
            request()->session()->put("cart", $cart);
        }

        //return redirect('/cart');
        return "Deleted";
    }

    public function update(Product $product) {


        if($product) {
            $cart = [];
            $cart['total'] = 0;
            $type = request()->type;

            if(request()->session()->has("cart")) {
                $cartOld = request()->session()->get("cart");
                $totalPrice = 0;
                foreach($cartOld as $key => $value) {
                    if($key != "total") {
                        if($key == $product->id) {
                            $value['amount'] += $type;
                        }
                        if($value['amount'] > 0) {
                            $cart[$key] = $value;
                            $totalPrice += $value['amount'] * $value['price'];
                        }
                    }
                }
                $cart["total"] = $totalPrice;

            }

            request()->session()->forget("cart");
            request()->session()->put("cart",$cart);

            /*$kosik = request()->session()->get("cart");
            foreach ($kosik as $i => $value) {
                if($i == "total") {
                    echo $i." ".$value . "<br>";
                }
                else {
                    echo $i." ".$value["amount"]." ".$value["price"]."<br>";
                }
            }*/

        }

        //return redirect('/cart');
        return "Kosik upraveny";

    }

}

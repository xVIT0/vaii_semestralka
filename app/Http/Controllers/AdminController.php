<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function index() {
        $products = Product::withTrashed()->paginate(20);
        return view('admin.index',compact('products'));
    }

    public function indexCategory() {
        $categories = Category::withTrashed()->paginate(20);
        return view('admin.category.indexCategory',compact('categories'));
    }

    public function indexOrder() {
        $orders = Order::paginate(20);
        return view('admin.order.indexOrder', compact('orders'));
    }

    public function showOrder(Order $order) {
        return view('admin.order.showOrder', compact('order'));
    }

    public function updateOrder() {
        $order = Order::find(request()->order_id)->update([
            'state' => 'vybavené',
        ]);
        return redirect('/admin/orders');
    }

    public function editCategory(Category $category) {
        return view('admin.category.editCategory', compact('category'));
    }

    public function destroyCategory() {
        $category = Category::find(request()->category_id)->delete();
        return redirect('/admin/categories');
    }

    public function restoreCategory() {
        $product = Category::withTrashed()->find(request()->category_id)->restore();
        return redirect('/admin/categories');
    }

    public function updateCategory(Category $category) {
        request()->validate([
            'name' => 'required',
        ]);

        $category->update([
            'name' => request()->name,
        ]);
        return redirect('/admin/categories');
    }

    public function createCategory() {
        return view('admin.category.createCategory');
    }

    public function storeCategory() {
        $category = Category::create(request()->validate([
            'name' => 'required',
        ]));
        return redirect('/admin/categories');
    }

    public function create() {
        $categories = Category::all();
        return view('admin.create', compact('categories'));
    }


    public function store() {
        request()->validate([
            'category_id' => 'required|numeric',
        ]);
        $product = Product::create(request()->validate([
            'category_id' => 'required|numeric',
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'image_path' => 'required|file|image',
        ]));
        $category = request()->category_id;

        $dest = public_path('/storage/product_images/');
        $img = request()->file('image_path');
        $image_name = Str::random(16) .'.'.$img->extension();
        $image = Image::make($img->path());
        $image->resize(1000, 600, function ($constraint) {
            $constraint->aspectRatio();
        })->save($dest . $image_name);
        $product->update([
            'image_path' => ('product_images/' . $image_name)
        ]);

        return redirect('/admin/products');
    }

    public function destroy() {
        $product = Product::find(request()->product_id)->delete();
        return redirect('/admin/products');
    }

    public function restore() {
        $product = Product::withTrashed()->find(request()->product_id)->restore();
        return redirect('/admin/products');
    }

    public function edit(Product $product) {
        $categories = Category::all();
        return view('admin.edit', compact('product', 'categories'));
    }

    public function update(Product $product) {
        request()->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'image_path' => 'sometimes|file|image',
            'category' => 'required|numeric'
        ]);

        $product->update([
            'name' => request()->name,
            'price' => request()->price,
            'description' => request()->description,
        ]);
        $categoryId = request()->category;
        $product->categories()->sync($categoryId);

        if (request()->image_path != null) {
            $temp = $product->image_path;
            $dest = public_path('/storage/product_images/');
            $img = request()->file('image_path');
            $image_name = Str::random(16) .'.'.$img->extension();
            $image = Image::make($img->path());
            $image->resize(1000, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->save($dest . $image_name);
            $product->update([
                'image_path' => ('product_images/' . $image_name)
            ]);
            Storage::disk('public')->delete($temp);
        }
        return redirect('/admin/products/');
    }
}

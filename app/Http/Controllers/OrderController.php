<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct() {
        $this->middleware("auth");
    }

    public function index() {
        $orders = Auth::user()->orders()->orderBy("created_at", "desc")->paginate(2);
        return view('order.index', compact('orders'));
    }

    public function store() {
        //dd(request());
        $cart = [];
        $products_id = [];

        if(request()->session()->has("cart")) {
            $cart = session("cart");
            foreach ($cart as $key => $value) {
                if($key != "total" && !Product::find($key)->deleted) {
                    array_push($products_id, $key);
                }
            }

        }
        $products = Product::find($products_id);
        //dd($products);
        $user = Auth::user();
        $order = $user->orders()->create([
            "name" => $user->name,
            "surname" => $user->surname,
            "price" => $cart['total'],
        ]);

        $syncData = [];
        foreach ($products as $product) {
            $syncData[$product->id] = [
                "price" => $cart[$product->id]["price"],
                "amount" => $cart[$product->id]["amount"]
            ];
        }
        $order->products()->syncWithoutDetaching($syncData);
        request()->session()->forget("cart");

        return redirect('/orders');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index () {
        $products = Product::all()->random(9);
        $categories = Category::all();
        return view("home", compact("products", "categories"));
    }

    public function show (Product $product){
        if($product->deleted)
            abort(404);
        $categories = Category::all();
        return view("product.show", compact("product","categories"));
    }

    public function showCategory (Category $category){
        $products= $category->products()->paginate(9);
        $categories = Category::all();
        return view("product.showCategory", compact("products", "category", "categories"));
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = [
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function orders() {
        return $this->belongsToMany(Order::class);
    }
}

-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: Po 17.Jan 2022, 23:13
-- Verzia serveru: 10.4.21-MariaDB
-- Verzia PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `semestralka`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notebooky', '2022-01-17 19:51:39', '2022-01-17 20:52:10', NULL),
(2, 'Mobilné telefóny', '2022-01-17 19:51:51', '2022-01-17 19:51:51', NULL),
(3, 'Počítače', '2022-01-17 20:16:23', '2022-01-17 20:16:23', NULL),
(4, 'Konzoly', '2022-01-17 20:16:39', '2022-01-17 20:16:39', NULL),
(5, 'Televízory', '2022-01-17 20:16:46', '2022-01-17 20:16:46', NULL),
(6, 'Slúchadlá', '2022-01-17 20:16:58', '2022-01-17 20:16:58', NULL),
(7, 'Mikrofóny', '2022-01-17 20:17:05', '2022-01-17 20:17:05', NULL),
(8, 'Rádiá', '2022-01-17 20:17:18', '2022-01-17 20:17:18', NULL),
(9, 'Práčky', '2022-01-17 20:17:32', '2022-01-17 20:17:32', NULL),
(10, 'Sušičky', '2022-01-17 20:17:36', '2022-01-17 20:18:05', NULL),
(11, 'Mrazničky', '2022-01-17 20:17:49', '2022-01-17 20:18:01', NULL),
(12, 'Vysávače', '2022-01-17 20:17:56', '2022-01-17 20:17:56', NULL);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_10_12_000000_create_users_table', 1),
(2, '2021_01_21_000001_create_category', 1),
(3, '2021_01_21_171732_create_products_table', 1),
(4, '2021_02_11_132359_create_orders_table', 1),
(5, '2021_02_11_134421_create_order_products_table', 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Spracováva sa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `orders`
--

INSERT INTO `orders` (`id`, `name`, `surname`, `price`, `user_id`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Filip', 'Vitkovský', 13360, 2, 'vybavené', '2022-01-17 19:53:13', '2022-01-17 19:58:15'),
(2, 'Filip', 'Vitkovský', 3484, 2, 'Spracováva sa', '2022-01-17 21:00:10', '2022-01-17 21:00:10'),
(3, 'Filip', 'Vitkovský', 51, 2, 'Spracováva sa', '2022-01-17 21:02:28', '2022-01-17 21:02:28');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `price` bigint(20) UNSIGNED NOT NULL,
  `amount` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `price`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 581, 23, '2022-01-17 19:53:13', '2022-01-17 19:53:13'),
(2, 2, 7, 1161, 3, '2022-01-17 21:00:10', '2022-01-17 21:00:10'),
(3, 3, 18, 51, 1, '2022-01-17 21:02:28', '2022-01-17 21:02:28');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `price`, `description`, `image_path`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'iPhone 11 64 GB čierny', 580.85, 'Mobilný telefón – 6.1\" IPS 1792 × 828, procesor Apple A13 Bionic 6-jadrový, RAM 4GB, interná pamäť 64GB, zadný fotoaparát s optickým zoomom 12Mpx (f/1.8)+12Mpx (f/2.4), predný fotoaparát 12Mpx, optická stabilizácia, GPS, Glonass, NFC, LTE, UWB, Lightning port, vodoodolný podľa IP68, single SIM + eSIM, neblokovaný, rýchle nabíjanie 18W, bezdrôtové nabíjanie, batéria 3110mAh, iOS 13', 'product_images/sYKqlF1UONw6F7wO.jpg', NULL, '2022-01-17 19:52:41', '2022-01-17 20:51:44'),
(2, 1, 'Lenovo Legion 5 Pro 16ACH6H kovový', 1129.78, 'Herný notebook – AMD Ryzen 5 5600H, 16\" IPS antireflexný 2560 × 1600 165Hz, RAM 16GB DDR4, NVIDIA GeForce RTX 3060 6GB 130 W, SSD 512GB, numerická klávesnica, podsvietená klávesnica, webkamera, USB-C, WiFi 6, 80 Wh batéria, hmotnosť 2.45kg, Bez operačného systému', 'product_images/w3ovpqUMt6kWxtV4.jpg', NULL, '2022-01-17 20:19:16', '2022-01-17 20:19:16'),
(3, 1, 'Macbook Air 13\" M1 Vesmírne sivý SK 2020', 1061.44, 'MacBook – Apple M1, 13.3\" IPS lesklý 2560 × 1600 , RAM 8GB, Apple M1 7-jadrová GPU, SSD 256GB, podsvietená klávesnica, webkamera, USB-C, čítačka odtlačkov prstov, WiFi 6, 49.9 Wh batéria, hmotnosť 1.25kg, macOS', 'product_images/tZsImQ1sBy3zVuIB.jpg', NULL, '2022-01-17 20:19:52', '2022-01-17 20:19:53'),
(4, 1, 'MacBook Pro 16\" M1 MAX International English 2021 Vesmírne sivý', 7289, 'MacBook – Apple M1 MAX, 16.2\" Liquid Retina XDR lesklý 3456 × 2234 120Hz, RAM 64GB, Apple M1 MAX 32jádrová GPU, SSD 8000GB, podsvietená klávesnica, webkamera, čítačka odtlačkov prstov, WiFi 6, hmotnosť 2.2kg, macOS', 'product_images/HgbRAOh4qI6D4IxD.png', NULL, '2022-01-17 20:20:50', '2022-01-17 20:20:50'),
(5, 2, 'Xiaomi Redmi Note 10 5G 128 GB modrý', 220.02, 'Mobilný telefón – 6.5\" IPS 2400 × 1080, 90Hz, procesor MediaTek MT6833 Dimensity 700 5G 8-jadrový, RAM 4GB, interná pamäť 128GB, Micro SDXC až 512 GB, zadný fotoaparát 48Mpx (f/1.8)+2Mpx (f/2.4)+2Mpx (f/2.4), predný fotoaparát 8Mpx, GPS, Glonass, IrDA, NFC, LTE, 5G, Jack (3.5mm) a USB-C, čítačka odtlačkov prstov, hybridný slot, neblokovaný, rýchle nabíjanie 18W, batéria 5000mAh, Android 11', 'product_images/Ggon8j921G7aduZG.png', NULL, '2022-01-17 20:22:02', '2022-01-17 20:22:02'),
(6, 2, 'Samsung Galaxy Z Fold3 5G 512GB zelený', 1934.26, 'Mobilný telefón – 7.6\" AMOLED 2208 × 1768, 120Hz, procesor Qualcomm Snapdragon 888 8-jadrový, RAM 12GB, interná pamäť 512GB, zadný fotoaparát s optickým zoomom 12Mpx (f/1.8)+12Mpx (f/2.4)+12Mpx (f/2.2), predný fotoaparát 10Mpx, optická stabilizácia, GPS, Glonass, NFC, LTE, 5G, UWB, USB-C, čítačka odtlačkov prstov, single SIM + eSIM, neblokovaný, rýchle nabíjanie 25W, bezdrôtové nabíjanie 11W, batéria 4400mAh, Android 11', 'product_images/wl4LLnJBM3Ni1uyz.png', NULL, '2022-01-17 20:22:41', '2022-01-17 20:22:41'),
(7, 3, 'HP Pavilion Gaming TG01-1001nc Black', 1161.31, 'Herný PC Intel Core i5 10400F Comet Lake 4.3 GHz, NVIDIA GeForce RTX 3060 12 GB, RAM 16GB DDR4, SSD 512GB, Bez mechaniky, WiFi, DVI a HDMI, 2×USB 3.2, 4×USB 2.0, typ skrine: Mini Tower, Myš a Klávesnica, Windows 11 Home (4R5E8EA)', 'product_images/7FfvA3nFQTETuyEc.png', NULL, '2022-01-17 20:23:47', '2022-01-17 20:23:47'),
(8, 3, 'ASUS ROG Strix GA35 G35DX-CZ020T Star Black', 2894.62, 'Herný PC AMD Ryzen 9 5900X 4.8 GHz, NVIDIA GeForce RTX 3080 10 GB, RAM 32GB DDR4, SSD 1000GB+ HDD 2TB 7200 ot./min, Bez mechaniky, WiFi, HDMI a DisplayPort, 6×USB 3.2, Windows 10 Home', 'product_images/sPblivQRqYCfDFhg.png', NULL, '2022-01-17 20:25:08', '2022-01-17 20:25:08'),
(9, 3, 'Alza TopOffice i5 SSD', 749, 'Počítač Intel Core i5 11400 Rocket Lake 4.4 GHz, Intel UHD Graphics 730, RAM 16GB DDR4, SSD 500GB, DVD, WiFi, VGA D-SUB, HDMI a DisplayPort, 2×USB 3.2, 4×USB 2.0, typ skrine: Midi Tower, Windows 11 Pro', 'product_images/TvXhavwFRPcieIcH.png', NULL, '2022-01-17 20:25:54', '2022-01-17 20:25:54'),
(10, 4, 'PlayStation 5 + Ratchet and Clank: Rift Apart', 554.9, 'Herná konzola – k TV, SSD 825 GB, Blu-ray (4K), možnosť hrania v 4K, menu v slovenčine, 1× herný ovládač –Ratchet and Clank: Rift Apart – momentálne bohužiaľ z dôvodu celosvetovej nedostupnosti nepoznáme konkrétny termín naskladnenia tovaru. Všetky objednávky budú vybavované postupne podľa termínu vytvorenia predobjednávky.', 'product_images/7vWXY6ywQJk7u6eg.png', NULL, '2022-01-17 20:26:48', '2022-01-17 20:26:48'),
(11, 4, 'Xbox Series S + Forza Horizon 5 Xbox Digital', 347.9, 'Herná konzola – k TV, SSD 512 GB, bez mechaniky (neprehráva fyzické disky), menu v slovenčine, 1× herný ovládač –Forza Horizon 5, hra (el. kľúč, dostupný v knižnici na Moja Alza), biela farba', 'product_images/kCGM9GwZV0zdZkmD.png', NULL, '2022-01-17 20:28:03', '2022-01-17 20:28:03'),
(12, 4, 'Nintendo Switch – Grey Joy-Con + Pokémon Legends: Arceus', 349.9, 'Herná konzola – hybridná (do ruky/k TV), 6.2 \" displej, HDD 32 GB, Slot na pamäťové karty, hra v balení (fyzický nosič)–Pokémon Legends: Arceus, kapacita batérie 4310 mAh, sivá farba', 'product_images/oevtfzfxpoXTIVnr.png', NULL, '2022-01-17 20:28:51', '2022-01-17 20:28:51'),
(13, 5, '58\" Philips The One 58PUS8505', 589, 'Televízor SMART ANDROID LED, 146cm, 4K Ultra HD, PPI 2100 (50Hz), Direct LED, HDR10, HDR10+, HLG, Dolby Vision, DVB-T2/S2/C, H.265/HEVC, 4× HDMI, 2× USB, USB nahrávanie, LAN, WiFi, Bluetooth, DLNA, HbbTV 2.0, herný režim, hlasové ovládanie, Ambilight, Apple TV, Netflix, HBO GO, Steam Link, Voyo, Google Assistant, prehrávanie 360° videa, Amazon Alexa, VESA 300×200, repro 20W, Dolby Atmos, Dolby Digital AC3, DTS HD, trieda energetickej účinnosti G', 'product_images/9JWnSczcJHh3e9BQ.png', NULL, '2022-01-17 20:29:45', '2022-01-17 20:29:45'),
(14, 5, '55\" Hisense 55A7500F', 439, 'Televízor SMART LED, 139cm, 4K Ultra HD, PCI Hisense 2000 (50Hz), Direct LED, HDR10+, HLG, Dolby Vision, DVB-T2/S2/C, H.265/HEVC, 3× HDMI, 2× USB, USB nahrávanie, LAN, WiFi, Bluetooth, DLNA, Miracast, HbbTV 2.0, herný režim, Netflix, Voyo, párovanie s mobilným zariadením, Vidaa, VESA 300×200, repro 20W, DTS Virtual: X, trieda energetickej účinnosti G', 'product_images/S79BAV2SDCbx9b3g.png', NULL, '2022-01-17 20:30:06', '2022-01-17 20:30:06'),
(15, 5, '50\" TCL 50C715', 457.54, 'Televízor SMART ANDROID QLED, 125cm, 4K Ultra HD, PPI 2400 (50Hz), Direct LED, HDR10, HDR10+, HLG, Dolby Vision, lokálne stmievanie, DVB-T2/S2/C, H.265/HEVC, 3× HDMI, 2× USB, LAN, WiFi, Bluetooth, DLNA, Chromecast, HbbTV 2.0, herný režim, hlasové ovládanie, Apple TV, Netflix, HBO GO, Steam Link, Voyo, Google Assistant, Amazon Alexa, párovanie s mobilným zariadením, VESA 200×200, repro 20W, Dolby Atmos, trieda energetickej účinnosti G', 'product_images/w2N5iVDSQNoOJtOu.png', NULL, '2022-01-17 20:30:29', '2022-01-17 20:30:30'),
(16, 6, 'Apple AirPods 2021', 199.9, 'Bezdrôtové slúchadlá s mikrofónom, True Wireless kôstky, uzatvorená konštrukcia, Bluetooth 5.0, hlasový asistent, prepínanie skladieb, prijímanie hovorov, certifikácia IPX4, výdrž batérie až 30h(6h+24h)', 'product_images/Eriavnqkx3WpMjTN.png', NULL, '2022-01-17 20:31:01', '2022-01-17 20:31:01'),
(17, 6, 'Jabra Evolve 20 MS Stereo USB-A', 42.16, 'Slúchadlá s mikrofónom, cez hlavu, na uši, uzatvorená konštrukcia, USB-A, s ovládaním hlasitosti, prijímanie hovorov, prepínanie skladieb, frekvenčný rozsah 20Hz–20000Hz, citlivosť 93,6 dB/mW, impedancia 32 Ohm, menič 28mm, kábel 1.2m', 'product_images/Mr7WtVJnCVNXsJZV.png', NULL, '2022-01-17 20:31:23', '2022-01-17 20:31:23'),
(18, 7, 'CONNECT IT ProMic CMI-9010-BK, čierny', 51.29, 'Mikrofón na statív, pripojenie USB, dĺžka kábla 2.5 m, kondenzátorový, smerové snímanie, frekvencia od 20 Hz do 16000 Hz, impedancia 150 Ohm, citlivosť -38 dB, pop filter', 'product_images/3KuHKisAqt66xnc0.png', NULL, '2022-01-17 20:32:02', '2022-01-17 20:32:02'),
(19, 7, 'FIFINE K669B', 25.68, 'Mikrofón stolný, pripojenie USB, dĺžka kábla 1.8 m, kondenzátorový, smerové snímanie, frekvencia od 20 Hz do 20000 Hz, impedancia 100 Ohm, citlivosť -34 dB', 'product_images/o2vqKnuACyCT0zBc.png', NULL, '2022-01-17 20:32:22', '2022-01-17 20:32:22'),
(20, 9, 'WHIRLPOOL TDLRB 65242BS EU/N+ 20 rokov záruka na Direct Drive motor po registrácii', 295.9, 'Práčka plnená zhora, energetická trieda C, biela farba, hlavné funkcie: odložený štart, rýchly program, displej a detská poistka, váž. spotreba vody na cyklus 44 l, kapacita práčky (program ECO 40–60°C): 6.5kg, váž. spotreba energie 57 kWh/100 cyklov, 1200ot./min pri odstreďovaní, špeciálne technológie: 6, zmysel, rozmery: 40×90×60 cm (Š×V×H), hmotnosť 56kg', 'product_images/jBrnDnUeBqy8RFWV.png', NULL, '2022-01-17 20:32:53', '2022-01-17 20:32:53'),
(21, 8, 'Hyundai TRC 788 AU3BBL čierno-modrý', 46.28, 'Rádiomagnetofón – prehrávané médiá: CD, kazeta a USB flash disk, FM tuner s 20 predvoľbami, s USB, so slúchadlovým výstupom 3,5 mm a so vstupom Jack 3,5 mm, napájanie: na baterky a zo siete, podpora MP3 a WMA, výkon 3 W, automatické ladenie', 'product_images/1gMrdc4xBz7txtx6.png', NULL, '2022-01-17 20:33:19', '2022-01-17 20:33:19'),
(22, 8, 'Sencor SRC 340', 22.1, 'Rádiobudík – s projekciou času na stenu, napájanie: na baterky, zo siete a záložná batéria, FM tuner s 10 predvoľbami, digitálny ukazovateľ času', 'product_images/Z2y2xpFhwMAxOnaj.png', NULL, '2022-01-17 20:33:43', '2022-01-17 20:33:43'),
(23, 10, 'BOSCH WTW85461BY', 541.02, 'Sušička prádla kondenzačná, energetická trieda A++, kapacita bielizne: 9kg, hlavné funkcie: napojenie na odpad, samočistiaci kondenzátor, odložený štart, displej, rýchly program a systém proti krčeniu bielizne, hlučnosť 64 dB, odhad. ročná spotreba energie 259 kWh/annum, špeciálne technológie: SensitiveDrying, biela farba, rozmery: 84.2×59.8×59.9 cm (V×Š×H), hmotnosť 50kg', 'product_images/I4rXdw9W4xs0MXK6.png', NULL, '2022-01-17 20:34:30', '2022-01-17 20:34:30'),
(24, 11, 'Amica FZ 208.3 AA', 282.37, 'Zásuvková mraznička energetickej triedy E, objem 146 l, klimatická trieda ST, biela farba, 125.2×54.5×59.7 cm (V×Š×H)', 'product_images/6mENInpUhNRx0yKA.png', NULL, '2022-01-17 20:35:01', '2022-01-17 20:35:01'),
(25, 12, 'Xiaomi Mi Robot Vacuum Mop 1C', 223.72, 'Robotický vysávač s mopom a na čistenie kobercov, na všetky druhy podláh, automatické dobíjanie, predvoľba miestností na upratovanie, pokračovanie upratovania po samostatnom dobití, senzor proti pádu zo schodov, senzor proti nárazu, vysávanie viacerých miestností, detekcia znečistených miest a prispôsobenie povrchu podlahy, navigácia: vSLAM, hepa filter, hlučnosť 60dB, mobilná aplikácia, nabíjacia stanica a nádobka na vodu na mokré čistenie, biela farba', 'product_images/L9qzuMMNc60DpLXL.png', NULL, '2022-01-17 20:36:05', '2022-01-17 20:36:05'),
(26, 12, 'Miele Triflex HX1 Pro', 779, 'Tyčový vysávač 3 v 1, pre alergikov a na čistenie kobercov, aku, na všetky druhy podláh, doba nabíjania 4h, typ batérie: Li-Ion, príkon 170W, doba chodu 120min, HEPA filter, objem nádržky 0.5l, príslušenstvo: držiak na stenu, hubica na čalúnenie, LED osvetlenie, rotačná kefa, sací štetec a štrbinová hubica, sivá farba', 'product_images/RmDcMO6YQksFcYFZ.png', NULL, '2022-01-17 20:36:36', '2022-01-17 20:36:36'),
(27, 12, 'Samsung Jet 90 complete', 369.02, 'Tyčový vysávač – pre alergikov 99,999% viacvrstvový filtračný systém, rýchle nabíjanie 3.5h, doba prevádzky 40min, vymeniteľná batéria, príkon 410W, objem nádržky 0.8 litra na čistenie kobercov, na zvieracie chlpy a antibakteriálny, na všetky druhy podláh, nabíjacia stanica 2v1 na stenu/stôl, mäkčené kolieska, regulácia výkonu 3 stupne', 'product_images/fLyjGuHG1Plhu1Ic.png', NULL, '2022-01-17 20:37:16', '2022-01-17 20:37:16'),
(28, 1, 'Asus ROG Zephyrus G15 GA503QS-HQ003T Moonlight White', 2631.77, 'Herný notebook – AMD Ryzen 9 5900HS, 15.6\" IPS antireflexný 2560 × 1440 165Hz, RAM 32GB DDR4, NVIDIA GeForce RTX 3080 8GB, SSD 1000GB, podsvietená klávesnica, USB 3.2 Gen 1, USB-C, čítačka odtlačkov prstov, WiFi 6, 90 Wh batéria, hmotnosť 1.9kg, Windows 10 Home', 'product_images/JnmLQxi7paepkE3s.png', NULL, '2022-01-17 20:38:26', '2022-01-17 20:38:27'),
(29, 1, 'Lenovo IdeaPad 3 15ALC6 Sand', 692.6, 'Notebook – AMD Ryzen 7 5700U, 15.6\" IPS antireflexný 1920 × 1080, RAM 16GB DDR4, AMD Radeon Graphics, SSD 1000GB, numerická klávesnica, podsvietená klávesnica, webkamera, USB 3.2 Gen 1, USB-C, čítačka odtlačkov prstov, WiFi 5, hmotnosť 1.65kg, Bez operačného systému', 'product_images/GtLh5m2ulxvExSdA.png', NULL, '2022-01-17 20:39:04', '2022-01-17 20:39:04'),
(30, 1, 'Lenovo ThinkPad E15 Gen 2 – ITU', 1066.96, 'Notebook – Intel Core i7 1165G7 Tiger Lake, 15.6\" IPS antireflexný 1920 × 1080, RAM 16GB DDR4, NVIDIA GeForce MX450 2GB, SSD 512GB, numerická klávesnica, podsvietená klávesnica, webkamera, USB 3.2 Gen 1, USB-C, čítačka odtlačkov prstov, WiFi 6, 45 Wh batéria, hmotnosť 1.7kg, Windows 10 Pro', 'product_images/3NywVN1rRQS12OXU.png', NULL, '2022-01-17 20:39:23', '2022-01-17 20:39:23'),
(31, 1, 'Acer Nitro 5 Shale Black', 910.9, 'Herný notebook – Intel Core i5 1130G7 Tiger Lake, 15.6\" IPS matný 1920 × 1080 144Hz, RAM 8GB DDR4, NVIDIA GeForce RTX 3050 4GB 75 W, SSD 512GB, podsvietená klávesnica, webkamera, USB 3.2 Gen 1, USB 3.2 Gen 2, WiFi 6, hmotnosť 2.2kg, Windows 11 Home, HDD upgrade kit (AN515-56-50F0)', 'product_images/UMhwnAwcbaxVA0Jz.png', NULL, '2022-01-17 20:40:06', '2022-01-17 20:40:06'),
(32, 1, 'Acer Aspire 3 Charcoal Black', 589, 'Notebook – AMD Ryzen 5 3500U, 15.6\" matný 1920 × 1080, RAM 8GB DDR4, AMD Radeon Vega 8, SSD 512GB, USB 3.2 Gen 1, WiFi 5, 36 Wh batéria, hmotnosť 1.9kg, Windows 10 Home (A315-23-R5S0)', 'product_images/oRDzFLfAzGXvkDvm.png', NULL, '2022-01-17 20:41:26', '2022-01-17 20:41:26'),
(33, 1, 'ASUS ZenBook 14 UX425EA-KI361T Pine Grey kovový', 809, 'Ultrabook – Intel Core i5 1135G7 Tiger Lake, 14\" IPS antireflexný 1920 × 1080, RAM 8GB LPDDR4X, Intel Iris Xe Graphics, SSD 512GB, podsvietená klávesnica, webkamera, USB-C, WiFi 6, hmotnosť 1.17kg, Windows 10 Home, MIL-STD 810G', 'product_images/RuBIhgrdA7aLoZXY.png', NULL, '2022-01-17 20:41:43', '2022-01-17 20:41:43'),
(34, 1, 'HP Pavilion Gaming 15-ec2900nc Shadow Black/Ghost White', 983.03, 'Herný notebook – AMD Ryzen 5 5600H, 15.6\" IPS matný 1920 × 1080, RAM 16GB DDR4, NVIDIA GeForce RTX 3050 4GB, SSD 512GB, numerická klávesnica, podsvietená klávesnica, webkamera, USB 3.2 Gen 1, USB-C, WiFi 6, hmotnosť 2.25kg, Windows 11 Home (53M45EA)', 'product_images/jRsU2otujRP7YWo9.png', NULL, '2022-01-17 20:42:04', '2022-01-17 20:42:04');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Admin', 'admin', 'admin@admin.com', '$2y$10$fZ5qB5oXf21dvRnYFpAVAui9XCAo/Tuk8iLFvK2rpuSxzQ0JHSPQK', NULL, '2022-01-17 19:51:05', '2022-01-17 19:51:05', 1),
(2, 'Filip', 'Vitkovský', 'vitkovskyfilip@gmail.com', '$2y$10$kXuN8Pf62N/N7TVprqzQAeo5oTsvis2vrygtIZDdGWQ9NckpdgXXu', 'l2R0vFQzbVIcta6Frkit88ZXl3RzlKLTO5R5LhF9B6PxYoFDEtY9jA13yhsk', '2022-01-17 19:52:57', '2022-01-17 19:52:57', 0);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexy pre tabuľku `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_product_order_id_foreign` (`order_id`),
  ADD KEY `order_product_product_id_foreign` (`product_id`);

--
-- Indexy pre tabuľku `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexy pre tabuľku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pre tabuľku `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pre tabuľku `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pre tabuľku `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pre tabuľku `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT pre tabuľku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Obmedzenie pre tabuľku `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

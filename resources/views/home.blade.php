@extends('layouts.app')

@section('content')
@include('zobrazKategorie')
        <div class="container-products">
        <div class="nazovKategorie">
            <h1>Naše produkty</h1>
        </div>
            <div class= "nieco">
                    @forelse($products as $product)
                            <div class="product-card">
                                <div>
                                    <div class="product-image"><img src="/storage/{{ $product->image_path }}" alt=""></div>
                                    <div class="product-info">
                                        <a href="product/{{ $product->id }}"><h3>{{ $product->name }}</h3></a>
                                        <p>Cena: {{ $product->price }} €</p>
                                        <h4>Kategória</h4>
                                        @if($product->category!=null)
                                        {{$product->category->name}}
                                        @endif
                                    </div>
                                </div>
                                <button type="button" class="btn btn-secondary" onclick="window.location.href='product/{{ $product->id }}'">Zobraz</button>
                            </div>
                    @empty
                        <p>Žiadne produkty</p>
                    @endforelse
            </div>
        </div>
    </div>
@endsection

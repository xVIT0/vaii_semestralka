@extends('layouts.app')


@section('content')
    <h1 class="zarovnanie">Košík</h1>

    @if(!$products->isEmpty())
    <table class="admin-products-table">
        <tr>
            <th>Názov produktu</th>
            <th>Počet kusov</th>
            <th>Cena</th>
            <th></th>
        </tr>
    @endif

        @forelse($products as $product)
            <tr class="product">
                <td>
                    <a href="/product/{{$product->id}}">{{$product->name}}</a>
                </td>
                <td >
                    <div class="updateCartTd">
                    <form class="updateCart" action="/cart/{{$product->id}}" method="POST">
                        @csrf
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="type" value="-1">
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button class="remove-button" type="submit">-1</button>
                    </form>
                    <p>{{$cart[$product->id]["amount"]}}ks</p>
                    <form class="updateCart" action="/cart/{{$product->id}}" method="POST">
                        @csrf
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="type" value="1">
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button class="add-button" type="submit">+1</button>
                    </form></div>
                </td>
                <td>
                    {{$cart[$product->id]["price"]}}€
                </td>
                <td class="delete">
                    <form class="removeFromCart" action="/cart/{{$product->id}}" method="POST">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit">Vymazať</button>
                    </form>
                </td>
            </tr>
        @empty
            <div>
                <p class = "zarovnanie">Prázdny košík</p>
            </div>
            
        @endforelse

    </table>


    @if(count($products) > 0)
    <div class="zarovnanie">
        <h3>Celkom : {{$cart["total"]}} €</h3>
    </div>
    <form class="zarovnanie" action="/orders" method="post">
            @csrf
            <button class="submit-button" type="submit">Objednať</button>
    </form>
    @endif

@endsection

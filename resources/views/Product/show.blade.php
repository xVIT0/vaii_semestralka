@extends('layouts.app')


@section('content')
    @include('zobrazKategorie')
        <div class="container1">
            <aside>
                <div class="main-image">
                    <img src="/storage/{{$product->image_path}}" alt="">
                </div>
            </aside>

            <aside>
                <div class="aside-header">
                    <h1> {{$product->name}} </h1>
                </div>
                <div class="description">
                    {{$product->description}}
                </div>
                <div class="price">{{$product->price}} €</div>
                <div class="left-buttons">
                    <form id="addToCartForm" action="/cart/{{$product->id}}" method="POST">
                        @csrf
                        <input type="hidden" id="product_id" value="{{$product->id}}">

                        @if(Auth::check())
                            @if(Auth::user()->role != 1)
                                <button type="submit" class="submit-button">Kúpiť</button>
                            @endif
                        @else
                            <p style="color: red">Pre nákup produktu sa musíte prihlásiť.</p>
                        @endif
                    </form>

                </div>
            </aside>
        </div>
    </div>
@endsection

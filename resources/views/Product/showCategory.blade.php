@extends('layouts.app')
@section('content')
    @include('zobrazKategorie')
    <div class="container-products">
        <div class="nazovKategorie">
            <h1 >{{$category->name}}</h1>
        </div>
        <div class="nieco">
            @forelse($products as $product)
                <div class="product-card">
                    <div class="product-image"><img src="/storage/{{ $product->image_path }}" alt=""></div>
                        <div class="product-info">
                            <a href="../{{ $product->id }}"><h3>{{ $product->name }}</h3></a>
                            <p>Cena: {{ $product->price }} €</p>                        
                        </div>
                        <button type="button" class="btn btn-secondary" onclick="window.location.href='../{{ $product->id }}'">Zobraz</button>
                    </div>
            @empty
                <p>Žiadne produkty</p>
            @endforelse
            
                </div>
        {{ $products->links()}}
        </div>
    </div>

@endsection

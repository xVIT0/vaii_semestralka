<h2 id="mobile">Zobraz kategórie</h2>
    <div class="products">
        <div class="categories">
            <h2>Kategórie</h2>
            <ul class="categories-flex">
                @forelse($categories as $category)
                    <li><a href="/product/category/{{$category->id}}">{{$category->name}}</a></li>
                @empty
                    <li>Žiadne kategórie</li>
                @endforelse
            </ul>
        </div>
@extends('layouts.app')

@section('content')
    <h1 style="text-align: center;">Pridanie produktu</h1>
    <form class="form-admin" action="/admin/products" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input">
            <label for="name">Názov produktu:</label>
            <input id="name" type="text" name="name" autocomplete="off" required>
        </div>
        <div class="input">
            <label for="price">Cena produktu:</label>
            <input id="price" type="number" step='0.01' name="price" autocomplete="off" required>
        </div>
        <div class="input">
            <label for="description">Popis produktu:</label>

            <input id="description" type="text" name="description" autocomplete="off" required>
        </div>
        <div class="input">
            <label for="image_path"> Obrázok produktu:</label>
            <input id="image_path" type="file" name="image_path" required>
        </div>
        <div class="input">
            <label for="category">Kategória</label>
            <select name="category_id" id="category_id" required>
                @forelse($categories as $category)
                    <option value="{{$category->id}}">{{ $category->name }}</option>
                @empty
                    <option></option>
                @endforelse
            </select>
        </div>
        <button class="submit-button" type="submit"> Pridaj produkt</button>
    </form>
@endsection

@extends('layouts.app')

@section('content')



    <h1 style="text-align: center;">Úprava produktu</h1>
    <form class="form-admin" action="/admin/products/{{$product->id}}" method="post" enctype="multipart/form-data">

        @csrf
        {{ method_field('PATCH') }}
        <div class="input">
            <label for="name">Názov produktu:</label>
            <input id="name" type="text" name="name" value="{{$product->name}}" autocomplete="off">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="input">
            <label for="price">Cena produktu:</label>
            <input id="price" type="text" name="price" value="{{$product->price}}" autocomplete="off">
            @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="input">
            <label for="description">Popis produktu:</label>
            <input id="description" type="text" name="description" value="{{$product->description}}" autocomplete="off">
            @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="input">
            <label for="image_path"> Obrázok produktu:</label>
            <input id="image_path" type="file" name="image_path">
            @error('image_path')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <img class="edit-img" src="/storage/{{$product->image_path}}" alt="">
        </div>
        <div class="input">
            <label for="category">Kategória</label>
            <select name="category" id="category">
                @forelse($categories as $category)
                    <option value="{{$category->id}}" @if($product->categories[0]->id == $category->id) selected @endif >{{ $category->name }}</option>
                @empty
                    <option></option>
                @endforelse
            </select>
        </div>

        <button class="submit-button" type="submit"> Zmeň</button>
    </form>

@endsection

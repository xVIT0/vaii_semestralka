@extends('layouts.app')

@section('content')
    <h1 class="zarovnanie">Produkty</h1>
    @if(!$products->isEmpty())
    <table class="admin-products-table">
        <tr>
            <th>Názov</th>
            <th>Kategória</th>
            <th>Cena</th>
            <th>Stav</th>
            <th></th>
            <th></th>
        </tr>
    @endif

    @forelse($products as $product)
        <tr>
            <td>{{$product->name}}</td>
            @if(isset($product->category))
            <td>{{$product->category->name}}</td>
            @else
            <td>Zahodené</td>
            @endif
            <td>{{$product->price}}</td>
            <td>
                @if($product->deleted_at != null)
                    Zahodené
                @else
                    OK
                @endif
            </td>
            <td class="update">
                <a href="/admin/products/{{$product->id}}/edit">  úprava </a>
            </td>

            
            @if($product->deleted_at == null)
            <td class="delete">
                <form action="/admin/products/delete" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <button type = "submit">zahodiť</button>
                </form>
            @endif

            @if($product->deleted_at != null)
            <td class="restore">
                <form action="/admin/products/restore" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type = "hidden" name = "product_id" value = "{{$product->id}}">
                    <button type = "submit">obnoviť</button>
                </form>
            @endif
            </td>
        </tr>
        @empty
        <div>
            <p class="zarovnanie">
                Žiadne produkty.
            </p>
        </div>
    @endforelse

    </table>


    <div class="add_item_button">
        <a class="btn btn-secondary" href="/admin/products/create"> Pridať produkt </a>
    </div>
    
    {{ $products->links()}}

@endsection

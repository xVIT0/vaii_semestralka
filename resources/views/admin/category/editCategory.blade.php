@extends('layouts.app')

@section('content')

    <h1 style="text-align: center;">Úprava kategórie</h1>
    <form class="form-admin" action="/admin/categories/{{$category->id}}" method="post">

        @csrf
        {{ method_field('PATCH') }}
        <div class="input">
            <label for="name">Názov kategórie:</label>
            <input id="name" type="text" name="name" value="{{$category->name}}" autocomplete="off">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button class="submit-button" type="submit"> Zmeň</button>
    </form>

@endsection

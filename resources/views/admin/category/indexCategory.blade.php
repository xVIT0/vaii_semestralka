@extends('layouts.app')

@section('content')
    <h1 class="zarovnanie">Kategórie</h1>
    @if(!$categories->isEmpty())
    <table class="admin-products-table">
        <tr>
            <th>Názov</th>
            <th>Stav</th>
            <th></th>
            <th></th>
        </tr>
    @endif

    @forelse($categories as $category)
        <tr>
            <td>{{$category->name}}</td>
            <td>
                @if($category->deleted_at == null)
                    OK
                @else
                    Zahodené
                @endif
            </td>
            <td class="update">
                <a href="/admin/categories/{{$category->id}}/edit">  úprava </a>
            </td>

            
            @if($category->deleted_at == null)
            <td class="delete">
                <form action="/admin/categories/delete" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="category_id" value="{{$category->id}}">
                    <button type = "submit">zahodiť</button>
                </form>
            @endif
            @if($category->deleted_at != null)
            <td class="restore">
                <form action="/admin/categories/restore" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type = "hidden" name = "category_id" value = "{{$category->id}}">
                    <button type = "submit">obnoviť</button>
                </form>
            @endif
            </td>
        </tr>

        

        @empty
        <div>
            <p class="zarovnanie">
                Žiadne kategórie.
            </p>
        </div>
    @endforelse
    </table>
    
    <div class="add_item_button">
        <a class="btn btn-secondary" href="/admin/categories/create"> Pridaj kategóriu </a>
    </div>
    

    {{ $categories->links()}}

@endsection

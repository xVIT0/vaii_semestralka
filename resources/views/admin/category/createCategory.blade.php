@extends('layouts.app')

@section('content')

    <h1 style="text-align: center;">Vytvorenie kategórie</h1>
    <form class="form-admin" action="/admin/categories/" method="post">

        @csrf
        {{ method_field('POST') }}
        <div class="input">
            <label for="name">Názov kategórie:</label>
            <input id="name" type="text" name="name" autocomplete="off">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button class="submit-button" type="submit"> Vytvor</button>
    </form>

@endsection

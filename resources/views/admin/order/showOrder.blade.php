@extends('layouts.app')

@section('content')

        <div class="order">
            <h2>Číslo objednávky: {{ $order->id }}</h2>
            <p>Meno: {{$order->name}}</p>
            <p>Priezvisko: {{$order->surname}}</p>
            <p>Objednané:</p>
            <ul>
                @forelse($order->products as  $product)
                    <li>
                        <p>{{$product->name}} - {{$product->pivot->amount}}ks - {{$product->pivot->price}}€</p>
                    </li>
                @empty
                    <li><p>Žiadne objednávky</p></li>
                @endforelse
            </ul>
            <p>Celkom: {{$order->price}}€</p>
            <p>Stav: {{$order->state}}</p>
                @if($order->state != "vybavené")
                    <form action="/admin/orders/" method="POST">
                        @csrf
                        <input type="hidden" value="{{$order->id}}" name="order_id">
                        <div class="right">
                            <button class = "btn btn-secondary">vybaviť</button>
                        </div>
                        
                    </form>
                @endif
        </div>

       
    

@endsection

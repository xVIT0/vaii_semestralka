@extends('layouts.app')

@section('content')
<h1 class="zarovnanie">Objednávky</h1>
    @if(!$orders->isEmpty())
    <table class="admin-products-table">
        <tr>
            <th>ID</th>
            <th>Meno</th>
            <th>Priezvisko</th>
            <th>Cena</th>
            <th>Stav</th>
            <th>Dátum vytvorenia</th>
            <th>Dátum vybavenia</th>
            <th></th>
        </tr>
    @endif

    @forelse($orders as $order)
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->name}}</td>
            <td>{{$order->surname}}</td>
            <td>{{$order->price}}</td>
            <td>{{$order->state}}</td>
            <td>{{$order->created_at->format('d.m.Y -- H:i:s ')}}</td>
            <td>{{$order->updated_at->format('d.m.Y -- H:i:s ')}}</td>
            <td class="show">
                <a href="/admin/orders/{{$order->id}}">zobraz</a>
            </td>
        </tr>

        @empty
        <div>
            <p class="zarovnanie">
                Žiadne objednávky.
            </p>
        </div>
    @endforelse
    </table>  

    {{ $orders->links()}}
    

@endsection

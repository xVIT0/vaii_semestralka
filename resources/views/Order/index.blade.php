@extends('layouts.app')

@section('content')
<h1 class= "zarovnanie">Objednávky</h1>
    @forelse($orders as  $order)
        @if($order->state == "vybavené")
        <div class="order_fullfilled">
        @else
        <div class="order">
        @endif
            <h2>Číslo objednávky: {{ $order->id }}</h2>
            <p>Meno: {{$order->name}}</p>
            <p>Priezvisko: {{$order->surname}}</p>
            <p>Objednané: {{$order->created_at}}</p>
            <ul>
                @forelse($order->products as  $product)
                    <li>
                        <p>{{$product->name}} - {{$product->pivot->amount}}ks - {{$product->pivot->price}}€</p>
                    </li>
                @empty
                    <li><p>Žiadne objednávky</p></li>
                @endforelse
            </ul>
            <p>Celkom: {{$order->price}}€</p>
            <p>Stav: {{$order->state}}</p>
        </div>
    @empty
        <div class = "zarovnanie">
            <p>Žiadne objednávky</p>
        </div>
        
    @endforelse

    {{$orders->links()}}
    

@endsection

$(document).ready(function() {
   // Pridavanie do kosika
    $('#addToCartForm').submit(function(event) {
        event.preventDefault();
        let product_id = $('#product_id').val();
        axios
            .post('/cart/' + product_id)
            .then(result => {
                alert("Pridane do kosika");
                location.reload();
            })
            .catch(error => {
                console.log(error);
            });
    });

    // Mazanie z kosika
    $('.removeFromCart').submit(function(event) {
        event.preventDefault();
        let product_id = $(event.target).find("input[name='product_id']").val();
        //console.log(product_id);
        axios
            .delete('/cart/' + product_id)
            .then(result => {
                alert("Vymazane z kosika");
                location.reload();
            })
            .catch(error => {
                console.log(error);
            });
    });

    // Update kosika
    $('.updateCart').submit(function(event) {
        event.preventDefault();
        let product_id = $(event.target).find("input[name='product_id']").val();
        let type = $(event.target).find("input[name='type']").val();
        let token = $('meta[name="csrf-token"]').attr('content');
        let formData = new FormData();
        formData.append('_token', token);
        formData.append('_method', 'PATCH');
        formData.append('type', type);
        axios ({
                method: 'POST',
                url: '/cart/' + product_id,
                data: formData
            })
            .then(result => {
                location.reload();
            })
            .catch(error => {
                console.log(error);
            });
    });

    $('#mobile').click(function () {
        $('.categories').toggleClass('open');
    })

});
